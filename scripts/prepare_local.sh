#!/usr/bin/env bash
set -e

eval $(docker-machine env -u)
docker-compose down

docker-compose build
docker-compose up -d

sleep 5

docker-compose run --rm py3calendard python3 manage.py migrate --noinput
