#!/usr/bin/env bash
set -e

eval $(docker-machine env -u)

docker-compose up -d database
docker-compose run --rm py3calendard python3 manage.py test