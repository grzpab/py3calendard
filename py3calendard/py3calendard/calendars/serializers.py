# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from drf_enum_field.serializers import EnumFieldSerializerMixin
from rest_framework import serializers

from py3calendard.calendars import models


class CalendarSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = models.Calendar


class CalendarAccessSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    class Meta(object):
        model = models.CalendarAccess
