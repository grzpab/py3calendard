# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from typing import Dict

from django.contrib.auth.models import User
from django.db.models import Q

from py3calendard.calendars import models


class CalendarService(object):
    @classmethod
    def filter_criteria(cls, user: User) -> Q:
        return (
            Q(user=user) |
            Q(accesses__user=user)
        )

    @classmethod
    def inject_user_from_request(cls, user: User, data: Dict) -> None:
        data['user'] = user.id


class CalendarAccessService(object):
    @classmethod
    def filter_criteria(cls, user: User) -> Q:
        return (
            Q(user=user) |
            Q(calendar__user=user)
        )

    @classmethod
    def check_access_for_create_and_update_methods(cls, user_id: int, calendar_id: int) -> bool:
        return models.Calendar.objects.filter(
            id=calendar_id,
            user__id=user_id,
        ).exists()

    @classmethod
    def check_access_for_delete_method(cls, user_id: int, calendar_access: models.CalendarAccess):
        return user_id == calendar_access.calendar.user.id
