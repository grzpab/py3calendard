# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from enumfields import IntEnum


class CalendarAccessLevel(IntEnum):
    READ_EVENTS = 1
    WRITE_ON_EVENTS = 2

    class Labels(object):
        READ_EVENTS = 'Permission to read the calendar content'
        WRITE_ON_EVENTS = 'Permission to modify the calendar content'
