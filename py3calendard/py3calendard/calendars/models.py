# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from colorful.fields import RGBColorField
from enumfields import EnumIntegerField

from django.contrib.auth.models import User
from django.db import models

from py3calendard.calendars.enums import CalendarAccessLevel


class Calendar(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    name = models.CharField(null=False, blank=False, max_length=255)
    color = RGBColorField(null=True)

    class Meta(object):
        unique_together = (
            ('user', 'name', ),
        )


class CalendarAccess(models.Model):
    calendar = models.ForeignKey(Calendar, on_delete=models.CASCADE, null=False, related_name='accesses')
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    access_level = EnumIntegerField(CalendarAccessLevel, null=False)

    class Meta(object):
        unique_together = (
            ('calendar', 'user', ),
        )
