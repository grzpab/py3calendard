# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from py3calendard.calendars.models import Calendar, CalendarAccess

admin.site.register(Calendar, CalendarAccess)
