# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.request import Request

from py3calendard.calendars import models
from py3calendard.calendars import serializers
from py3calendard.calendars import services


class CalendarView(viewsets.ModelViewSet):
    queryset = models.Calendar.objects.all()
    serializer_class = serializers.CalendarSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def filter_queryset(self, queryset):
        user = self.request.user
        queryset = super(CalendarView, self).filter_queryset(queryset)
        return queryset.filter(services.CalendarService.filter_criteria(user))

    def create(self, request, *args, **kwargs) -> Response:
        # TODO gpp: I don't like it
        services.CalendarService.inject_user_from_request(
            user=request.user,
            data=request.data,
        )

        return super(CalendarView, self).create(
            request=request,
            args=args,
            kwargs=kwargs,
        )


class CalendarAccessView(viewsets.ModelViewSet):
    queryset = models.CalendarAccess.objects.all()
    serializer_class = serializers.CalendarAccessSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def filter_queryset(self, queryset):
        user = self.request.user
        queryset = super(CalendarAccessView, self).filter_queryset(queryset)
        return queryset.filter(services.CalendarAccessService.filter_criteria(user))

    def create(self, request, *args, **kwargs) -> Response:
        if not self._check_access_for_create_and_update_operations(request):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return super(CalendarAccessView, self).create(
            request=request,
            args=args,
            kwargs=kwargs,
        )

    def update(self, request, *args, **kwargs) -> Response:
        if not self._check_access_for_create_and_update_operations(request):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return super(CalendarAccessView, self).update(
            request=request,
            args=args,
            kwargs=kwargs,
        )

    def destroy(self, request, *args, **kwargs) -> Response:
        user_id = request.user.id
        instance = self.get_object()

        if not services.CalendarAccessService.check_access_for_delete_method(
            user_id=user_id,
            calendar_access=instance,
        ):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        self.perform_destroy(instance)

        return Response(status=status.HTTP_204_NO_CONTENT)

    @classmethod
    def _check_access_for_create_and_update_operations(cls, request: Request):
        return services.CalendarAccessService.check_access_for_create_and_update_methods(
            user_id=request.user.id,
            calendar_id=request.data['calendar'],
        )
