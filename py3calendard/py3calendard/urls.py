# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework.authtoken import views
from rest_framework.routers import DefaultRouter

from django.conf.urls import url

from py3calendard.calendars.views import CalendarView
from py3calendard.calendars.views import CalendarAccessView
from py3calendard.events.views import GlobalEventView, LocalEventView
from py3calendard.users.views import UserCreateAPIView
from py3calendard.users.views import UserListViewSet

event_list = {
    'get': 'list',
    'post': 'create'
}

event_detail = {
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy',
}

global_event_list = GlobalEventView.as_view(actions=event_list)
global_event_detail = GlobalEventView.as_view(actions=event_detail)

local_event_list = LocalEventView.as_view(actions=event_list)
local_event_detail = LocalEventView.as_view(actions=event_detail)

urlpatterns = [
    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^signup/$', UserCreateAPIView.as_view(), name='signup'),
    url(r'^users/$', UserListViewSet.as_view(), name='users'),

    url(r'^global-events/$',
        global_event_list,
        name='global-event-list'),
    url(r'^global-events/(?P<pk>[0-9]+)/$',
        global_event_detail,
        name='global-event-detail'),
    url(r'^global-events-by/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d+)/(?P<zone>-?\d*\.{0,1}\d+)/$',
        global_event_list,
        name='global-event-list-by-ymd'),
    url(r'^global-events-by/(?P<year>\d{4})/(?P<month>\d{2})/(?P<zone>-?\d*\.{0,1}\d+)/$',
        global_event_list,
        name='global-event-list-by-ym'),
    url(r'^global-events-by/(?P<year>\d{4})/(?P<zone>-?\d*\.{0,1}\d+)/$',
        global_event_list,
        name='global-event-list-by-y'),

    url(r'^local-events/$',
        local_event_list,
        name='local-event-list'),
    url(r'^local-events/(?P<pk>[0-9]+)/$',
        local_event_detail,
        name='local-event-detail'),
    url(r'^local-events-by/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d+)/(?P<zone>-?\d*\.{0,1}\d+)$',
        local_event_list,
        name='local-event-list-by-ymd'),
    url(r'^local-events-by/(?P<year>\d{4})/(?P<month>\d{2})/(?P<zone>-?\d*\.{0,1}\d+)$',
        local_event_list,
        name='local-event-list-by-ym'),
    url(r'^local-events-by/(?P<year>\d{4})/(?P<zone>-?\d*\.{0,1}\d+)$',
        local_event_list,
        name='local-event-list-by-y'),
]

router = DefaultRouter()
router.register(r'calendars', CalendarView,
                base_name='calendars')
router.register(r'calendar-accesses', CalendarAccessView,
                base_name='calendar-accesses')

urlpatterns += router.urls
