# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.generics import get_object_or_404

from py3calendard.events import serializers
from py3calendard.events import models
from py3calendard.events import services


class GlobalEventView(viewsets.ModelViewSet):
    queryset = models.GlobalEvent.objects.all()
    serializer_class = serializers.GlobalEventReadingSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def get_serializer_class(self):
        if self.action == 'create':
            return serializers.GlobalEventCreationSerializer

        return super(GlobalEventView, self).get_serializer_class()

    def get_object(self):
        if self.action in ['update', 'destroy', ]:
            return self._get_object_for_update_or_deletion()

        return super(GlobalEventView, self).get_object()

    def get_queryset(self):
        queryset = super(GlobalEventView, self).get_queryset()
        return services.EventService.get_event_queryset(queryset, self.kwargs)

    def filter_queryset(self, queryset):
        queryset = super(GlobalEventView, self).filter_queryset(queryset)

        return services.GlobalEventService.filter_queryset(
            user=self.request.user,
            queryset=queryset,
            is_readonly=True,
        )

    def create(self, request, *args, **kwargs):
        services.GlobalEventService.inject_user_from_request(
            user=request.user,
            data=request.data
        )

        return super(GlobalEventView, self).create(request, *args, **kwargs)

    def _get_object_for_update_or_deletion(self):
        queryset = services.GlobalEventService.filter_queryset(
            user=self.request.user,
            queryset=self.get_queryset(),
            is_readonly=False,
        )

        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        self.check_object_permissions(self.request, obj)

        return obj


class LocalEventPermission(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        permission = super(LocalEventPermission, self).has_permission(request=request, view=view)

        if not permission:
            return False

        user = request.user

        if view.action == 'create':
            g_event_id = request.data['global_event']

            if not services.LocalEventService.has_access_for_creation(user.id, g_event_id):
                return False

        elif view.action == 'retrieve':
            local_event = view.get_object()

            if not services.LocalEventService.has_access_for_reading(user, local_event):
                return False

        elif view.action == 'update':
            local_event = view.get_object()

            if not services.LocalEventService.has_access_for_update(user, local_event):
                return False

        elif view.action == 'destroy':
            local_event = view.get_object()

            if not services.LocalEventService.has_access_for_deletion(user, local_event):
                return False

        return True


class LocalEventView(viewsets.ModelViewSet):
    queryset = models.LocalEvent.objects.all()
    serializer_class = serializers.LocalEventForGlobalOwnerSerializer
    permission_classes = (LocalEventPermission, )

    def get_queryset(self):
        queryset = super(LocalEventView, self).get_queryset()
        return services.EventService.get_event_queryset(queryset, self.kwargs)

    def get_serializer_class(self):
        if self.action == 'create':
            return serializers.LocalEventCreationForGlobalOwnerSerializer
        elif self.action == 'retrieve':
            user = self.request.user
            local_event = self.get_object()

            if local_event.user.id == user.id:
                return serializers.LocalEventForLocalOwnerSerializer

            if local_event.global_event.user.id == user.id:
                return serializers.LocalEventForGlobalOwnerSerializer

        elif self.action == 'update':
            return serializers.LocalEventForLocalOwnerSerializer

        return super(LocalEventView, self).get_serializer_class()