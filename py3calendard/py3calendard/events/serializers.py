# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.models import User
from django.utils.translation import ugettext
from drf_enum_field.fields import EnumField

from drf_enum_field.serializers import EnumFieldSerializerMixin
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from py3calendard.events import enums
from py3calendard.events import models
from py3calendard.events import services


class LocalEventForLocalOwnerSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    global_event = serializers.PrimaryKeyRelatedField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta(object):
        model = models.LocalEvent


class LocalEventForGlobalOwnerSerializer(LocalEventForLocalOwnerSerializer):
    global_event = serializers.PrimaryKeyRelatedField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    rsvp = EnumField(required=False, enum_type=enums.Rsvp, default=enums.Rsvp.UNKNOWN)

    class Meta(object):
        model = models.LocalEvent
        fields = ('id', 'user', 'global_event', 'rsvp',)


class LocalEventCreationForGlobalOwnerSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):

    class Meta(object):
        model = models.LocalEvent
        fields = ('id', 'user', 'global_event', )

    def save(self, **kwargs):
        global_event = self.validated_data['global_event']

        self.validated_data['rsvp'] = enums.Rsvp.UNKNOWN

        self.validated_data['title'] = global_event.title
        self.validated_data['description'] = global_event.description
        self.validated_data['event_type'] = global_event.event_type
        self.validated_data['starting_date_time'] = global_event.starting_date_time
        self.validated_data['ending_date_time'] = global_event.ending_date_time
        self.validated_data['day'] = global_event.day

        return super(LocalEventCreationForGlobalOwnerSerializer, self).save(**kwargs)


class GlobalEventReadingSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    local_events = LocalEventForGlobalOwnerSerializer(
        many=True,
        required=False,
        read_only=True,
    )

    class Meta(object):
        model = models.GlobalEvent


class GlobalEventCreationSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    local_events = LocalEventForGlobalOwnerSerializer(
        many=True,
        required=False,
        read_only=True
    )

    users = serializers.ListField(
        child=serializers.PrimaryKeyRelatedField(
            queryset=User.objects.all()
        ),
        required=False,
    )

    class Meta(object):
        model = models.GlobalEvent

    def validate(self, attrs):
        starting_date_time = attrs.get('starting_date_time', None)
        ending_date_time = attrs.get('ending_date_time', None)
        day = attrs.get('day', None)

        event_type = attrs.get('event_type')

        if event_type == enums.EventType.PERIOD:
            if starting_date_time is not None and ending_date_time is not None:
                if starting_date_time > ending_date_time:
                    raise ValidationError(ugettext(
                        'The starting date time must be lower than '
                        'the ending date time.'
                    ))
            else:
                raise ValidationError(ugettext(
                    'The start and the end of the period must be defined.'
                ))
        else:
            if day is None:
                raise ValidationError('The day must be defined.')

        return attrs

    def save(self, **kwargs):
        users = self.validated_data.pop('users', [])

        instance = super(GlobalEventCreationSerializer, self).save(**kwargs)

        services.GlobalEventService.create_local_events(
            global_event=instance,
            users=users,
        )

        return instance
