# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from datetime import datetime
from datetime import tzinfo
from datetime import timedelta
from datetime import date

from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.db.models import Q

from py3calendard.calendars.models import CalendarAccessLevel
from py3calendard.calendars.models import CalendarAccess
from py3calendard.events.enums import Rsvp
from py3calendard.events.models import LocalEvent
from py3calendard.events.models import GlobalEvent


class EventService(object):

    @classmethod
    def get_event_queryset(cls, queryset, kwargs):
        year = kwargs.get('year', None)
        month = kwargs.get('month', None)
        day = kwargs.get('day', None)
        zone = kwargs.get('zone', None)

        if year is not None:
            if month is not None:
                if day is not None:
                    return cls.ymd_queryset(queryset, year, month, day, zone)

                return cls.ym_queryset(queryset, year, month, zone)

            return cls.y_queryset(queryset, year, zone)

        return queryset

    @classmethod
    def get_time_zone(cls, zone):
        class TimeZone(tzinfo):
            def utcoffset(self, dt):
                return timedelta(hours=int(zone) if zone is not None else 0)

        return TimeZone()

    @classmethod
    def ymd_queryset(cls, queryset, year, month, day, zone):
        year = int(year)
        month = int(month)
        day = int(day)

        starting_date_time = datetime(
            year=year,
            month=month,
            day=day,
            tzinfo=cls.get_time_zone(zone=zone)
        )

        ending_date_time = starting_date_time + relativedelta(days=1)

        date_ = date(year=year, month=month, day=day)

        return queryset.filter(
            Q(
                starting_date_time__gte=starting_date_time,
                starting_date_time__lte=ending_date_time,
            ) |
            Q(
                ending_date_time__gte=starting_date_time,
                ending_date_time__lte=ending_date_time,
            ) |
            Q(day=date_)
        )

    @classmethod
    def ym_queryset(cls, queryset, year, month, zone):
        year = int(year)
        month = int(month)
        day = 1

        starting_date_time = datetime(
            year=year,
            month=month,
            day=day,
            tzinfo=cls.get_time_zone(zone=zone),
        )

        ending_date_time = starting_date_time + relativedelta(months=1)

        starting_date = date(year=year, month=month, day=day)
        ending_date = starting_date + relativedelta(months=1)

        return cls._get_periodic_queryset(
            queryset=queryset,
            starting_date_time=starting_date_time,
            ending_date_time=ending_date_time,
            starting_date=starting_date,
            ending_date=ending_date,
        )

    @classmethod
    def y_queryset(cls, queryset, year, zone):
        year = int(year)
        month = 1
        day = 1

        starting_date_time = datetime(
            year=int(year),
            month=month,
            day=day,
            tzinfo=cls.get_time_zone(zone=zone),
        )

        ending_date_time = starting_date_time + relativedelta(years=1)

        starting_date = date(year=year, month=month, day=day)
        ending_date = starting_date + relativedelta(years=1)

        return cls._get_periodic_queryset(
            queryset=queryset,
            starting_date_time=starting_date_time,
            ending_date_time=ending_date_time,
            starting_date=starting_date,
            ending_date=ending_date,
        )

    @classmethod
    def _get_periodic_queryset(cls, queryset, starting_date_time,
                               ending_date_time, starting_date, ending_date):
        """
        :type starting_date_time: datetime.datetime
        :type ending_date_time: datetime.datetime
        :type starting_date: datetime.date
        :type ending_date: datetime.date
        """

        return queryset.filter(
            Q(
                starting_date_time__gte=starting_date_time,
                starting_date_time__lte=ending_date_time,
            ) |
            Q(
                ending_date_time__gte=starting_date_time,
                ending_date_time__lte=ending_date_time,
            ) |
            Q(
                day__gte=starting_date,
                day__lte=ending_date,
            )
        )


class GlobalEventService(object):

    @classmethod
    def create_local_events(cls, global_event, users):
        for user in users:
            if user.pk == global_event.user.pk:
                continue

            LocalEvent.objects.create(
                user=user,
                title=global_event.title,
                description=global_event.description,
                event_type=global_event.event_type,
                starting_date_time=global_event.starting_date_time,
                ending_date_time=global_event.ending_date_time,
                day=global_event.day,
                global_event=global_event,
                rsvp=Rsvp.UNKNOWN,
            )

    @classmethod
    def inject_user_from_request(cls, user, data):
        data['user'] = user.id

    @classmethod
    def filter_queryset(cls, user, queryset, is_readonly):
        if is_readonly:
            access_level_sub_query = (
                Q(access_level=CalendarAccessLevel.READ_EVENTS) |
                Q(access_level=CalendarAccessLevel.WRITE_ON_EVENTS)
            )
        else:
            access_level_sub_query = (
                Q(access_level=CalendarAccessLevel.WRITE_ON_EVENTS)
            )

        calendar_ids = CalendarAccess.objects.filter(
            Q(user=user) & access_level_sub_query
        ).values_list('calendar', flat=True)

        return queryset.filter(
            Q(user=user) |
            Q(calendar__user=user) |
            Q(calendar__id__in=calendar_ids)
        )


class LocalEventService(object):

    @classmethod
    def has_access_for_creation(cls, user_id: int, global_event_id: int) -> bool:
        return GlobalEvent.objects.filter(id=global_event_id, user__id=user_id).exists()

    @classmethod
    def has_access_for_reading(cls, user: User, local_event: LocalEvent) -> bool:
        return cls.has_access_for_deletion(user, local_event)

    @classmethod
    def has_access_for_update(cls, user: User, local_event: LocalEvent) -> bool:
        return user.id == local_event.user.id

    @classmethod
    def has_access_for_deletion(cls, user, local_event) -> bool:
        return user.pk in [local_event.user.id, local_event.global_event.user.id]
