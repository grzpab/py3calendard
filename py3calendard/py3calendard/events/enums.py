# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from enumfields import IntEnum


class EventType(IntEnum):
    ALL_DAY = 1
    PERIOD = 2

    class Labels(object):
        ALL_DAY = 'The event is going to take place the entire day'
        PERIOD = 'The event is going to take place within a stated period.'


class Rsvp(IntEnum):
    UNKNOWN = 1
    NO = 2
    MAYBE = 3
    YES = 4

    class Labels(object):
        UNKNOWN = 'The response to the invitation is unknown.'
        NO = 'The invitation has been rejected.'
        MAYBE = 'The invitee might participate in the event.'
        YES = 'The invitation has been accepted'
