# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from py3calendard.events.models import GlobalEvent, LocalEvent

admin.site.register(GlobalEvent, LocalEvent)
