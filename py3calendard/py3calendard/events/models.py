# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.models import User
from django.db import models
from enumfields import EnumIntegerField

from py3calendard.events import enums


class Event(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    title = models.CharField(null=False, blank=False, max_length=255)
    description = models.CharField(null=True, blank=True, max_length=255)
    event_type = EnumIntegerField(enums.EventType, null=False)
    starting_date_time = models.DateTimeField(null=True)
    ending_date_time = models.DateTimeField(null=True)
    day = models.DateField(null=True)

    class Meta(object):
        abstract = True


class GlobalEvent(Event):
    calendar = models.ForeignKey('calendars.Calendar')


class LocalEvent(Event):
    global_event = models.ForeignKey(GlobalEvent, null=False, related_name='local_events')
    rsvp = EnumIntegerField(enums.Rsvp, null=False)
