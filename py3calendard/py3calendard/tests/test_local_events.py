# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import datetime
from rest_framework.test import APITestCase

from py3calendard.events.enums import EventType
from py3calendard.events.enums import Rsvp

from py3calendard.tests.utils import Calendar
from py3calendard.tests.utils import CalendarAdapter
from py3calendard.tests.utils import GlobalEvent
from py3calendard.tests.utils import GlobalEventAdapter
from py3calendard.tests.utils import LocalEventAdapter
from py3calendard.tests.utils import User
from py3calendard.tests.utils import UserManager
from py3calendard.tests.utils import LocalEvent


class TestLocalEvents(APITestCase):
    def setUp(self):
        super(TestLocalEvents, self).setUp()

        self.user1 = User.create()
        self.user2 = User.create()
        self.user3 = User.create()

        self.user1_manager = UserManager(client=self.client, user=self.user1)
        self.user2_manager = UserManager(client=self.client, user=self.user2)
        self.user3_manager = UserManager(client=self.client, user=self.user3)

        self.user1_manager.create()
        self.user2_manager.create()
        self.user3_manager.create()

    def test_local_events(self):
        today = datetime.date.today()

        with self.user1_manager.do_as_user():
            calendar = Calendar(name='My first calendar')
            calendar_adapter = CalendarAdapter(
                client=self.client,
                calendar=calendar,
            )

            calendar_adapter.create()

            global_event = GlobalEvent(
                title='Title',
                description='Description',
                event_type=EventType.ALL_DAY,
                starting_date_time=None,
                ending_date_time=None,
                day=today,
                calendar_pk=calendar.pk,
            )

            global_event_adapter = GlobalEventAdapter(
                client=self.client,
                global_event=global_event,
            )

            global_event_adapter.create(users=[])

            local_event = LocalEvent(
                user=self.user2,
                global_event=global_event,
            )

            local_event_adapter = LocalEventAdapter(
                client=self.client,
                local_event=local_event,
            )

        with self.user2_manager.do_as_user():
            with self.assertRaises(Exception):
                local_event_adapter.create()

        with self.user1_manager.do_as_user():
            response_data = local_event_adapter.create()

            self.assertDictEqual(response_data, {
                'id': response_data['id'],
                'user': self.user2.pk,
                'global_event': global_event.pk,
            })

            response_data = local_event_adapter.read()
            self.assertDictEqual(response_data, {
                'id': response_data['id'],
                'user': self.user2.pk,
                'global_event': global_event.pk,
                'rsvp': 'UNKNOWN',
            })

        with self.user2_manager.do_as_user():
            response_data = local_event_adapter.read()
            self.assertDictEqual(response_data, {
                'id': response_data['id'],
                'user': self.user2.pk,
                'global_event': global_event.pk,
                'title': 'Title',
                'description': 'Description',
                'event_type': 'ALL_DAY',
                'starting_date_time': None,
                'ending_date_time': None,
                'day': today.isoformat(),
                'rsvp': 'UNKNOWN',
            })

        with self.user3_manager.do_as_user():
            with self.assertRaises(Exception):
                local_event_adapter.read()

        with self.user1_manager.do_as_user():
            with self.assertRaises(Exception):
                local_event_adapter.update()

        with self.user2_manager.do_as_user():
            response_data = local_event_adapter.update(
                title='My title',
                description='My description',
                event_type=EventType.PERIOD,
                starting_date_time='2016-08-08T04:12:51.679911-07:00',
                ending_date_time='2016-08-08T04:12:51.679911-07:00',
                rsvp=Rsvp.YES,
            )

            self.assertDictEqual(response_data, {
                'event_type': 'PERIOD',
                'title': u'My title',
                'ending_date_time': '2016-08-08T04:12:51.679911-07:00',
                'day': None,
                'user': self.user2.pk,
                'rsvp': 'YES',
                'starting_date_time': '2016-08-08T04:12:51.679911-07:00',
                'global_event': global_event.pk,
                'id': local_event.pk,
                'description': 'My description',
            })

        with self.user3_manager.do_as_user():
            with self.assertRaises(Exception):
                local_event_adapter.update()

        # deletion
        with self.user3_manager.do_as_user():
            with self.assertRaises(Exception):
                local_event_adapter.delete()

        with self.user2_manager.do_as_user():
            local_event_adapter.delete()

        with self.user1_manager.do_as_user():
            with self.assertRaises(Exception):
                # it really does not exist anymore
                local_event_adapter.delete()

                local_event_adapter.local_event = local_event
                local_event_adapter.create()

                local_event_adapter.delete()

                # when the global event is deleted, so should be the local one
                global_event_adapter.delete()

                with self.assertRaises(Exception):
                    local_event_adapter.read()
