# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from typing import Dict

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from py3calendard.calendars import models
from py3calendard.tests.utils.commons import RestApiAdapter


class Calendar(object):
    def __init__(self, name: str, color: str=None):
        self.name = name
        self.color = color
        self.user_pk = None
        self.pk = None


class CalendarAccess(object):
    def __init__(self, user, calendar: Calendar, access_level: models.CalendarAccessLevel):
        self.user = user
        self.calendar = calendar
        self.access_level = access_level
        self.pk = None


class CalendarAdapter(RestApiAdapter):
    def __init__(self, client: APIClient, calendar: Calendar):
        self.client = client
        self.calendar = calendar

    def create(self) -> Dict:
        data = {
            'name': self.calendar.name,
        }

        if self.calendar.color is not None:
            data['color'] = self.calendar.color

        response = self.client.post(
            path=reverse('calendars-list'),
            data=data,
            format='json'
        )

        if response.status_code != status.HTTP_201_CREATED:
            raise Exception(response.json())

        response_json = response.json()

        self.calendar.pk = response_json['id']
        self.calendar.user_pk = response_json['user']

        return response_json

    def update(self, name: str=None, color: str=None) -> Dict:
        data = {
            'id': self.calendar.pk,
            'user': self.calendar.user_pk,
            'name': self.calendar.name if name is None else name,
            'color': self.calendar.color if color is None else color,
        }

        response = self.client.put(
            path=reverse('calendars-detail', kwargs={'pk': self.calendar.pk}),
            data=data,
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        response_json = response.json()

        self.calendar.name = response_json['name']
        self.calendar.color = response_json['color']

        return response_json

    @staticmethod
    def read_all(client: APIClient) -> Dict:
        response = client.get(reverse('calendars-list'), format='json')

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        return response.json()

    def read(self) -> Dict:
        response = self.client.get(
            path=reverse('calendars-detail', kwargs={'pk': self.calendar.pk}),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    def delete(self) -> None:
        response = self.client.delete(
            path=reverse('calendars-detail', kwargs={'pk': self.calendar.pk}),
            format='json'
        )

        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise Exception(response.json())

        self.calendar = None


class CalendarAccessAdapter(RestApiAdapter):
    def __init__(self, client: APIClient, calendar_access: CalendarAccess):
        self.client = client
        self.calendar_access = calendar_access

    def create(self) -> Dict:
        data = {
            'user': self.calendar_access.user.pk,
            'calendar': self.calendar_access.calendar.pk,
            'access_level': self.calendar_access.access_level.name,
        }

        response = self.client.post(
            path=reverse('calendar-accesses-list'),
            data=data,
            format='json'
        )

        if response.status_code != status.HTTP_201_CREATED:
            raise Exception(response.data)

        self.calendar_access.pk = response.data['id']

        return response.data

    def update(self, access_level: models.CalendarAccessLevel) -> Dict:
        data = {
            'id': self.calendar_access.pk,
            'user': self.calendar_access.user.pk,
            'calendar': self.calendar_access.calendar.pk,
            'access_level': access_level.name,
        }

        response = self.client.put(
            path=reverse('calendar-accesses-detail', kwargs={
                'pk': self.calendar_access.pk
            }),
            data=data,
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        self.calendar_access.access_level = access_level

        return response.data

    def delete(self) -> None:
        response = self.client.delete(
            path=reverse('calendar-accesses-detail', kwargs={
                'pk': self.calendar_access.pk
            }),
            format='json'
        )

        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise Exception(response.json())

        self.calendar_access = None
