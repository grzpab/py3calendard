# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from datetime import datetime, date
from typing import Optional, Union, Iterable, Dict

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from py3calendard.events.enums import EventType
from py3calendard.tests.utils.commons import RestApiAdapter


class GlobalEvent(object):
    def __init__(self, title: str, description: str, event_type: EventType,
                 starting_date_time: Optional[Union[datetime, str]],
                 ending_date_time: Optional[Union[datetime, str]],
                 day: Optional[date], calendar_pk: int, user_pk: int=None):
        self.title = title
        self.description = description
        self.event_type = event_type
        self.starting_date_time = starting_date_time
        self.ending_date_time = ending_date_time
        self.day = day
        self.calendar_pk = calendar_pk
        self.user_pk = user_pk
        self.pk = None


class GlobalEventAdapter(RestApiAdapter):
    def __init__(self, client: APIClient, global_event: GlobalEvent):
        self.client = client
        self.global_event = global_event

    def create(self, users: Optional[Iterable[int]]) -> Dict:
        data = self._get_create_or_update_payload(users=users)

        response = self.client.post(
            path=reverse('global-event-list'),
            data=data,
            format='json',
        )

        if response.status_code != status.HTTP_201_CREATED:
            raise Exception(response.json())

        response_json = response.json()

        self.global_event.pk = response_json['id']
        self.global_event.user_pk = response_json['user']

        return response.json()

    def update(self, title: str=None, description: str=None, event_type: EventType=None,
               starting_date_time: Union[datetime, str, None]=None,
               ending_date_time: Union[datetime, str, None]=None,
               day: Optional[date]=None, calendar_pk: Optional[int]=None) -> Dict:
        data = self._get_create_or_update_payload(
            title=title,
            description=description,
            event_type=event_type,
            starting_date_time=starting_date_time,
            ending_date_time=ending_date_time,
            day=day,
            calendar_pk=calendar_pk,
        )

        response = self.client.put(
            path=reverse(
                'global-event-detail',
                kwargs={
                    'pk': self.global_event.pk,
                },
            ),
            data=data,
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        response_json = response.json()

        global_event = self.global_event

        global_event.title = response_json['title']
        global_event.description = response_json['description']
        global_event.event_type = response_json['event_type']
        global_event.starting_date_time = response_json['starting_date_time']
        global_event.ending_date_time = response_json['ending_date_time']
        global_event.day = response_json['day']
        global_event.calendar_pk = response_json['calendar']

        return response_json

    def read(self) -> Dict:
        response = self.client.get(
            path=reverse(
                'global-event-detail',
                kwargs={
                    'pk': self.global_event.pk,
                },
            ),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        return response.json()

    @staticmethod
    def read_all(client: APIClient) -> Dict:
        response = client.get(
            path=reverse('global-event-list'),
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        return response.json()

    @staticmethod
    def read_all_by_year(client: APIClient, date_: date, zone: str) -> Dict:
        response = client.get(
            path=reverse(
                'global-event-list-by-y',
                kwargs={
                    'year': date_.strftime('%Y'),
                    'zone': zone,
                },
            ),
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        return response.json()

    @staticmethod
    def read_all_by_month(client: APIClient, date_: date, zone: str) -> Dict:
        response = client.get(
            path=reverse(
                'global-event-list-by-ym',
                kwargs={
                    'year': date_.strftime('%Y'),
                    'month': date_.strftime('%m'),
                    'zone': zone,
                },
            ),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        return response.json()

    @staticmethod
    def read_all_by_date(client: APIClient, date_: date, zone: str) -> Dict:
        response = client.get(
            path=reverse(
                'global-event-list-by-ymd',
                kwargs={
                    'year': date_.strftime('%Y'),
                    'month': date_.strftime('%m'),
                    'day': date_.strftime('%d'),
                    'zone': zone,
                },
            ),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.json())

        return response.json()

    def delete(self) -> None:
        response = self.client.delete(
            path=reverse(
                'global-event-detail',
                kwargs={
                    'pk': self.global_event.pk,
                },
            ),
            format='json',
        )

        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise Exception(response.json())

        self.global_event = None

    def _get_create_or_update_payload(self, title: str=None, description: str=None,
                                      event_type: EventType=None,
                                      starting_date_time: Optional[Union[datetime, str]]=None,
                                      ending_date_time: Optional[Union[datetime, str]]=None,
                                      day: Optional[date]=None, calendar_pk: Optional[int]=None,
                                      users: Optional[Iterable[int]]=None):
        if title is None:
            title = self.global_event.title

        if description is None:
            description = self.global_event.description

        if event_type is None:
            event_type = self.global_event.event_type

        if calendar_pk is None:
            calendar_pk = self.global_event.calendar_pk

        data = {
            'title': title,
            'description': description,
            'event_type': event_type.name,
            'calendar': calendar_pk
        }

        if event_type == EventType.ALL_DAY:
            day = self.global_event.day if day is None else day

            data['day'] = day
            data['starting_date_time'] = None
            data['ending_date_time'] = None
        elif event_type == EventType.PERIOD:
            if starting_date_time is None:
                starting_date_time = self.global_event.starting_date_time

            if ending_date_time is None:
                ending_date_time = self.global_event.ending_date_time

            data['day'] = None
            data['starting_date_time'] = starting_date_time
            data['ending_date_time'] = ending_date_time
        else:
            raise Exception('Unsupported EventType')

        if self.global_event.pk is not None:
            data['id'] = self.global_event.pk

        if self.global_event.user_pk:
            data['user'] = self.global_event.user_pk,

        if users is not None:
            data['users'] = users

        return data
