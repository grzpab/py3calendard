# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import time
from datetime import datetime, date
from typing import Optional, Dict, Union

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from py3calendard.events.enums import EventType, Rsvp
from py3calendard.tests.utils import GlobalEvent
from py3calendard.tests.utils.users import User
from py3calendard.tests.utils.commons import RestApiAdapter


def local_time_offset(t=None):
    """Return offset of local zone from GMT, either at present or at time t."""
    # python2.3 localtime() can't take None
    if t is None:
        t = time.time()

    if time.localtime(t).tm_isdst and time.daylight:
        return -time.altzone
    else:
        return -time.timezone


class LocalEvent(object):
    def __init__(self, user: User, global_event: GlobalEvent, title: Optional[str]=None,
                 description: Optional[str]=None, event_type: Optional[EventType]=None,
                 starting_date_time: Optional[datetime]=None, ending_date_time: Optional[datetime]=None,
                 day: Optional[date]=None, rsvp: Optional[Rsvp]=None):
        self.user = user
        self.global_event = global_event

        self.title = title
        self.description = description
        self.event_type = event_type
        self.starting_date_time = starting_date_time
        self.ending_date_time = ending_date_time
        self.day = day
        self.rsvp = rsvp

        self.pk = None


class LocalEventAdapter(RestApiAdapter):
        def __init__(self, client: APIClient, local_event: LocalEvent):
            self.client = client
            self.local_event = local_event

        def create(self) -> Dict:
            data = {
                'user': self.local_event.user.pk,
                'global_event': self.local_event.global_event.pk
            }
            response = self.client.post(
                path=reverse('local-event-list'),
                data=data,
                format='json'
            )

            if response.status_code != status.HTTP_201_CREATED:
                raise Exception(response.json())

            self.local_event.pk = response.data['id']

            return response.data

        def read(self) -> Dict:
            response = self.client.get(
                path=reverse(
                    'local-event-detail',
                    kwargs={
                        'pk': self.local_event.pk,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.json())

            return response.json()

        @staticmethod
        def read_all(client: APIClient) -> Dict:
            response = client.get(
                path=reverse('local-event-list'),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.json())

            return response.json()

        @staticmethod
        def read_all_by_year(client: APIClient, date_: date, zone: str) -> Dict:
            response = client.get(
                path=reverse(
                    'local-event-list-by-y',
                    kwargs={
                        'year': date_.strftime('%Y'),
                        'zone': zone,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.json())

            return response.json()

        @staticmethod
        def read_all_by_month(client: APIClient, date_: date, zone: str) -> Dict:
            response = client.get(
                path=reverse(
                    'local-event-list-by-ym',
                    kwargs={
                        'year': date_.strftime('%Y'),
                        'month': date_.strftime('%m'),
                        'zone': zone
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.json())

            return response.json()

        @staticmethod
        def read_all_by_date(client: APIClient, date_: date, zone: str) -> Dict:
            response = client.get(
                path=reverse(
                    'local-event-list-by-ymd',
                    kwargs={
                        'year': date_.strftime('%Y'),
                        'month': date_.strftime('%m'),
                        'day': date_.strftime('%d'),
                        'zone': zone,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.json())

            return response.json()

        def update(self, title: Optional[str]=None, description: Optional[str]=None,
                   event_type: Optional[EventType]=None,
                   starting_date_time: Union[datetime, str, None]=None,
                   ending_date_time: Union[datetime, str, None]=None,
                   day: Optional[date]=None, rsvp: Optional[Rsvp]=None) -> Dict:
            data = self._get_update_payload(
                title=title,
                description=description,
                event_type=event_type,
                starting_date_time=starting_date_time,
                ending_date_time=ending_date_time,
                day=day,
                rsvp=rsvp,
            )

            response = self.client.put(
                path=reverse(
                    'local-event-detail',
                    kwargs={
                        'pk': self.local_event.pk,
                    },
                ),
                data=data,
                format='json')

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.json())

            response_json = response.json()

            self.local_event.title = response_json['title']
            self.local_event.description = response_json['description']
            self.local_event.event_type = response_json['event_type']
            self.local_event.starting_date_time = response_json['starting_date_time']
            self.local_event.ending_date_time = response_json['ending_date_time']
            self.local_event.day = response_json['day']
            self.local_event.rsvp = response_json['rsvp']

            return response_json

        def delete(self) -> None:
            response = self.client.delete(
                path=reverse(
                    'local-event-detail',
                    kwargs={
                        'pk': self.local_event.pk,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_204_NO_CONTENT:
                raise Exception(response.json())

            self.local_event = None

        def _get_update_payload(self, title: Optional[str]=None, description: Optional[str]=None,
                                event_type: Optional[EventType]=None, starting_date_time: Optional[datetime]=None,
                                ending_date_time: Optional[datetime]=None, day: Optional[date]=None,
                                rsvp: Optional[Rsvp]=None) -> Dict:
            if title is None:
                title = self.local_event.title

            if description is None:
                description = self.local_event.description

            if event_type is None:
                event_type = self.local_event.event_type

            if rsvp is None:
                rsvp = self.local_event.rsvp

            data = {
                'id': self.local_event.pk,
                'user': self.local_event.user.pk,
                'title': title,
                'description': description,
                'event_type': event_type.name,
                'rsvp': rsvp.name,
            }

            if event_type == EventType.ALL_DAY:
                day = self.local_event.day if day is None else day

                data['day'] = day
                data['starting_date_time'] = None
                data['ending_date_time'] = None
            elif event_type == EventType.PERIOD:
                if starting_date_time is None:
                    starting_date_time = self.local_event.starting_date_time

                if ending_date_time is None:
                    ending_date_time = self.local_event.ending_date_time

                data['day'] = None
                data['starting_date_time'] = starting_date_time
                data['ending_date_time'] = ending_date_time
            else:
                raise Exception('Unsupported EventType')

            return data
