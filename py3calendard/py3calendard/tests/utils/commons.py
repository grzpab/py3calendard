# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from typing import Dict

from rest_framework.test import APIClient


class RestApiAdapter(object):
    def create(self, *args, **kwargs) -> Dict:
        raise NotImplemented

    def read(self, *args, **kwargs) -> Dict:
        raise NotImplemented

    def update(self, *args, **kwargs) -> Dict:
        raise NotImplemented

    def delete(self, *args, **kwargs) -> None:
        raise NotImplemented

    @staticmethod
    def read_all(client: APIClient) -> Dict:
        raise NotImplemented
