# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from contextlib import contextmanager
from typing import Dict

from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APIClient


class User(object):
    def __init__(self, username: str, email: str, password: str):
        self.username = username
        self.email = email
        self.password = password
        self.pk = None

    @staticmethod
    def create():
        faker = Faker()

        return User(
            username=faker.last_name()+faker.last_name(),
            email=faker.email(),
            password=faker.password()
        )


class UserManager(object):
    def __init__(self, client: APIClient, user: User):
        self.client = client
        self.user = user

    @contextmanager
    def do_as_user(self):
        """Log in, do some operations and log out."""

        if self.user.pk is None:
            self.create()

        self._login()

        yield

        self._logout()

    def create(self) -> None:
        path = reverse('signup')
        response = self.client.post(
            path,
            data=self._get_creation_payload(),
            format='json'
        )

        if response.status_code != status.HTTP_201_CREATED:
            raise Exception(response.data)

        self.user.pk = response.data['id']

    def _login(self) -> None:
        result = self.client.login(
            username=self.user.username,
            password=self.user.password
        )

        if result is False:
            raise Exception('The logging-in action was unsuccessful')

    def _logout(self) -> None:
        self.client.logout()

    def _get_creation_payload(self) -> Dict:
        return {
            'username': self.user.username,
            'email': self.user.email,
            'password': self.user.password,
            'password_confirmation': self.user.password,
        }

