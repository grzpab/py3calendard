# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from py3calendard.tests.utils import User
from py3calendard.tests.utils import UserManager


class TestUsers(APITestCase):
    def setUp(self):
        self.user = User.create()
        self.user_manager = UserManager(
            client=self.client,
            user=self.user,
        )

        self.user_manager.create()

    def test_unauthorized_access_to_user_list(self):
        response = self.client.get(reverse('users'), format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.json(), {'detail': 'Authentication credentials were not provided.'})

    def test_authorized_access_to_user_list(self):
        with self.user_manager.do_as_user():
            response = self.client.get(reverse('users'), format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            response_json = response.json()
            self.assertEqual(len(response_json), 1)

            self.assertListEqual(response_json, [{
                'pk': self.user.pk,
                'username': self.user.username,
                'email': self.user.email,
            }])
