# py3calendard

A project of a simple calendar API utilising Python 3 and Docker.

Based on a project named **pyCalendar**, written in Python 2, which was not Dockerised.

## System requirements
* python 3.6+
* pip3
* Docker 1.13+

## Used technologies
* Django 1.10
* Django REST Framework 3.x

The project is currently using a PostgreSQL 9.6 database.
There are no environmental settings prepared, it runs as if it was a production environment.

### Setup for the IDEs not supporting Docker
```
cd py3calendard # main git directory
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

### Overall setup
```
./scripts/prepare_local.sh
```

### Tests
The tests are located in files starting with `test_.py` in the `tests` directory
The tests utilities can be found within `pycalendar.tests.utils` module.
```
./scripts/prepare_local.sh
```

### Translations
```
docker-compose run --rm py3calendard bash  # connect to the bash on the container
django-admin makemessages -l pl # or any other country code
django-admin compilemessages
```

Please take into account that, as a convention, the `*.mo` files are not persisted within the git repository.

### Running
```
docker-compose up -d
```
The server is not operating with HTML documents or payloads.
Please use always JSON-based requests.

## API Documentation
The endpoints, if not stated otherwise, follow the standard REST API principles:

* the `GET` method is used for retrieving either a list or a single document,
* the `POST` method is used for creating a new entry,
* the `PUT` method is used to make a non-partial update of a document,
* the `PATCH` method is used to make a partial update of a document,
* the `DELETE` method is used to delete a document permanently.

The tests follow the doc-tests convention. If anytime in doubt, please check your requests against the examples.

### Users:
User is created by accessing the `/signup/` POST endpoint with the following payload:

```
{
    'username': '?string',
    'email': '?string'
    'password': '?string',
    'password_confirmation': '?string'
}
```

In return, the server should respond with `200 OK` and return:
```
{
    'pk': '?integer',
    'username': '?string',
    'email': '?string'
}
```

From this moment on, a user is expected to log in since all the other endpoints require authentication.
There are two possible authorisation backends:

* basic authorisation,
* token authorisation.

The endpoint responsible for token authorisation is `/api-token-auth/`.

The list of users might be obtained from the `/users/` endpoint.
The update and deletion is not supported from the API.

### Calendars
The calendar endpoint is `/calendars/`. The payload is as presented:
```
{
    'id': '?integer',
    'user': '?string', # always the current user when creating
    'name': '?string',
    'color': '?string' # RRGGBB color, not obligatory
}
```

The access to the calendar is defined at `/calendar-accesses/`. The expected payload is as follows:

```
{
    'id': '?integer',
    'user': '?integer',
    'calendar': '?calendar',
    'access_level': '?integer' # 1 or READ_EVENTS = reading access, 2 or WRITE_ON_EVENTS = writing access
}
```

If a user is granted the reading access of the calendar, he or she can read all the global events.
If a user is granted the writing access, he or she can modify the global events.
The calendar owner is always granted the writing access without any access entries needed.

### Global events
If a user creates an event within a calendar, it becomes a global event.
The endpoint for global event is located at `/global-events/`. The desired payload is as presented:
```
{
    'id': '?integer',
    'user': '?integer',
    'calendar': '?integer',
    'title': '?string',
    'description': '?string',
    'event_type': '?' ALL_DAY = 1, PERIOD = 2
    'day': '?iso-format-date',
    'starting_date_time': '?iso-format-datetime',
    'ending_date_time': '?iso-format-datetime',
    'local_events': [ # only when READING
        {
            'id': '?integer',
            'user': '?integer',
            'rsvp': '?', # UNKNOWN = 1, NO = 2, MAYBE = 3, YES = 4
            'global_event': '?integer'
        },
    ],
    'users' = [] # only when creating - for which users should we create local events
}
```

### Local events
The local event is a copy of a global event for each invited user.
The respective endpoint is `/local-events/`. The payload looks as defined here:
```
{
    'id': '?integer',
    'user': '?integer',
    'global_event': '?integer',
    'title': '?string',
    'description': '?string',
    'event_type': '?' ALL_DAY = 1, PERIOD = 2
    'starting_date_time': '?iso-format-datetime',
    'ending_date_time': '?iso-format-datetime',
    'day': '?iso-format-date',
    'rsvp': '?' # UNKNOWN = 1, NO = 2, MAYBE = 3, YES = 4
}
```
The local event can be created only by the global event creator.
The local event can be modified only by the invitee.
The local event can be deleted either by the global event creator or the invitee.

### The batch reading of global or local events
It is possible to read all global or local events by a year, a month, or a particular day with two endpoints:

* `/local-events-by/`
* `/global-events-by/`

by proving YY (year), MM (month), DD (day) and (-) ZZ (timezone), like:

* `/local-events-by/2000/-7`
* `/local-events-by/2000/01/-8`
* `/local-events-by/2000/01/10/0`

All processing is done in with proper timezone adjustment.
